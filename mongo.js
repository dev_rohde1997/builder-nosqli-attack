 const MongoClient = require('mongodb').MongoClient;

 // Connection URL
const url = 'mongodb://mongo:27017';

 // Database Name
 const dbName = 'mydb';

module.exports = {
	connect: async function () {
		const client = await MongoClient.connect(url);
		console.log("Connected successfully to server");
		if(client != null)
			return client;
		client.close();
	},
	closeCon: async function (client) {
		client.close();
	},
	find: async function(callback) { 
        console.log('Starting MongoDB search...');
        const client = await MongoClient.connect(url);
		const db = client.db(dbName);
		// Get the documents collection
		const collection = await db.collection('users');
		// Find some documents
		var found = await collection.find();
		found.toArray()
		.then(function(docs) {
			//console.log("Database result:");
			//console.log(docs);
            console.log("Search finished.");
			callback(docs);
		})
		.catch(function(error) {
			console.log(error);
		});
		client.close();
	},
	find: async function(query, collection_name, callback) {
        console.log('Starting MongoDB search...', query);
        const client = await MongoClient.connect(url);
		const db = client.db(dbName);
		// Get the documents collection
		const collection = await db.collection(collection_name);
		// Find some documents
		var found = await collection.find(query);
		found.toArray()
		.then(function(docs) {
			console.log("Database result:");
			console.log(docs);
            console.log("Search finished.");
			callback(docs);
		})
		.catch(function(error) {
			console.log(error);
		});
		client.close();
	},
	insert: async function(collection_name, json) {
        console.log('Starting MongoDB insertion...');
        const client = await MongoClient.connect(url);
		const db = client.db(dbName);
		const collection = await db.collection(collection_name);
		collection.insertOne(json)
		.then(function(res){
			console.log("1 document successfully inserted.");
			console.log("Insertion finished.");
			return res;
		})
		.catch(function (error) {
			console.log(error);
		});
		client.close();
	},
    delete: async function() {
        console.log('Starting MongoDB delete...');
        const client = await MongoClient.connect(url);
        const db = client.db(dbName);
		const collection = await db.collection('users');
        collection.remove();
        console.log('MongoDB is now empty!');
		client.close();
    }
	
	
}