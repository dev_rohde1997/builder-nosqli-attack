const express = require('express');
const app = express();
const morgan = require('morgan')
const mongo = require('./mongo.js');

var path = require('path');

//CONSTANTS
const CREDENTIAL_COLLECTION_NAME = "users";
//Connect
mongo.connect();

//Clear old data
mongo.delete();

//Init an user
mongo.insert(CREDENTIAL_COLLECTION_NAME, {user: "admin", password: "1234"});
mongo.insert(CREDENTIAL_COLLECTION_NAME, {user: "moritz", password: "password"});
mongo.insert(CREDENTIAL_COLLECTION_NAME, {user: "felix", password: "qwerty"});
mongo.insert(CREDENTIAL_COLLECTION_NAME, {user: "dima", password: "secure"});


//Body parser
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.set('json replacer', 1);
app.set('json spaces', 40);


app.get('/recover-password', async (req, res) => {
  res.sendFile(path.join(__dirname + '/public/recover-password.html'));
});

app.post('/api/recover-password', async (req, res) => {
	console.log(req.body);
    try {
        query = {'user': JSON.parse(req.body.username)};
    } catch (e) {
        query = {'user': req.body.username};
    }
	await mongo.find(query, CREDENTIAL_COLLECTION_NAME, (users) => {
    console.log("Anzahl der Suchergebnisse: " + users.length)
        if(users.length > 0) {
            users.forEach(user => {
                    res
                        .status(200)
                        .cookie({token: "e8198c33b3d1b0748fa3db5cd6821d33951d67c8"})
                        .send(`Hello ${user.user}, \n your password is ${user.password}`)
            });
        } else {
            res.status(404).send("Invalid credentials");
        }
  });
});

app.get('/', async (req, res) => {
  res.status(301).redirect('/recover-password');
})

app.get('/check', async (req, res) => {
  mongo.find('', CREDENTIAL_COLLECTION_NAME, result => {
    res.status(200).json(result);
  })
});

app.use(morgan('dev'))

const port = 3000;
app.listen(port, () => console.log('Server running...')); 