# NoSQLI Attack

Basic application to demonstrate some NoSQLI attacks.

A **node.js** server is used with express. As database a NoSQL db **mongo** is used.

## Dependencies

- Docker
- docker-compose 
- node.js
- npm

Docker daemon must be running!
Ports `80` and `27017` must not be allocated!


## Usage

1. Clone repository
```
git clone git@gitlab.com:dev_rohde1997/builder-nosqli-attack.git
```

2. Navigate into repository
```
cd builder-nosqli-attack
```

3. Locally installation of node modules

*Required for hot loading*
```
npm install
```

4. Build application
```
docker-compose build
```

5. Start application
```
docker-compose up 
```

## Routes

### /recover-password

Website for test case

### /api/recover-password

Route used for the attack 

### /api/check
Get a list of valid credentials. (*only used during development*)
